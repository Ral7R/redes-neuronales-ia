﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuHandler : MonoBehaviour
{
    public Button go_btn;
    public Button learning_btn;
    public Button quit_btn;

    private void Start()
    {
        go_btn.onClick.AddListener(delegate () { LoadLevel("Battleground"); });
        learning_btn.onClick.AddListener(delegate () { LoadLevel("Menu"); });
        quit_btn.onClick.AddListener(delegate () { LoadLevel("Quit"); });
    }

    public void LoadLevel(string level)
    {
        if(level == "Quit")
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
        else
        {
            SceneManager.LoadScene(level);
        }
    }
}
